require('dotenv').config()
const  { expect, stash, spec } = require('pactum')
const  { usuarioRandom, cadastrarUsuario, consultarUsuario }  = require('../../requests/user.request')
const baseUrl = process.env.PACTUM_REQUEST_BASE_URL

describe('Testes da API - GET /Usuários/{id} - Listar usuário cadastrados', () => {
    let response
    stash.addDataTemplate({'User': usuarioRandom()})

    
    it('Validar a consulta de usuário existente', async () => {
        responseCadUsuario = await cadastrarUsuario()
        response = await consultarUsuario(responseCadUsuario.body._id)
        
        expect(response).to.have.status(200)
        expect(response).to.have.bodyContains('email')
        expect(response).to.have.bodyContains('nome')
        expect(response).to.have.bodyContains('password')
        expect(response).to.have.bodyContains('administrador')
        expect(response).to.have.bodyContains('_id')
    });
    
    it('Validar a consulta de id inexistente', async () => {
        response = await consultarUsuario('fwegz4aSGnxpz')
        
        expect(response).to.have.status(400)
        expect(response).to.have.bodyContains('Usuário não encontrado')
    });

    it('Validar o metódo inexistente na API', async () => {
        await spec()
        .patch(`${baseUrl}/usuarios/${response.body._id}`)
        .withHeaders('Content-Type', 'application/json')
        .withJson({ '@DATA:TEMPLATE@': 'User' })
        .expectStatus(405)
    });
});